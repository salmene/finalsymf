<?php
// src/AppBundle/Entity/User.php

namespace usertestBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Null;

/**
 * @ORM\Entity(repositoryClass="usertestBundle\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser


















{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(name="passwordGmail",type="string",length=255,nullable=true)
     */
    private $passwordGmail;
    /**
     * @Assert\NotNull
     * @ORM\Column(name="DateNaissance",type="string",length=255,nullable=true)
     */
    private $DateNaissance;
    /**
     * @ORM\Column(name="NumTel",type="string",length=255,nullable=true)
     */
    private $NumTel;
    /**
     * @Assert\NotNull
     * @ORM\Column(name="agence",type="string",length=255,nullable=true)
     */
    private $agence;
    /**
     * @Assert\NotNull
     * @ORM\Column(name="type",type="string",length=255,nullable=true)
     */
    private $type;
    /**
     * @Assert\NotNull
     * @ORM\Column(name="path",type="string",length=255,nullable=true)
     */
    private $path;


    public function __construct()
    {
        parent::__construct();

    }


    /**
     * @return mixed
     */
    public function getDateNaissance()
    {
        return $this->DateNaissance;
    }

    /**
     * @param mixed $DateNaissance
     */
    public function setDateNaissance($DateNaissance)
    {
        $this->DateNaissance = $DateNaissance;
    }

    /**
     * @return mixed
     */
    public function getNumTel()
    {
        return $this->NumTel;
    }

    /**
     * @param mixed $NumTel
     */
    public function setNumTel($NumTel)
    {
        $this->NumTel = $NumTel;
    }

    /**
     * @return mixed
     */
    public function getAgence()
    {
        return $this->agence;
    }

    /**
     * @param mixed $agence
     */
    public function setAgence($agence)
    {
        $this->agence = $agence;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getPasswordGmail()
    {
        return $this->passwordGmail;
    }

    /**
     * @param mixed $passwordGmail
     */
    public function setPasswordGmail($passwordGmail)
    {
        $this->passwordGmail = $passwordGmail;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }



}