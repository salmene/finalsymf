<?php

/**
 * Created by PhpStorm.
 * User: HP
 * Date: 11/02/2019
 * Time: 09:49
 */
namespace OffreBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use usertestBundle\Entity\User;


/**
 * @ORM\Entity(repositoryClass="OffreBundle\Repository\OffreRepository")
 * @ORM\Table(name="Offre")
 */
class Offre
{
    /**
     * @var int
     *
     * @ORM\Column(name="idOffre", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $Id;
    /**
     * @ORM\Column(name="type_offre",type="string",length=255)
     */
    private $typeOffre;
    /**
     * @ORM\Column(name="Description_offre",type="string",length=255)
     */
    private $descriptionOffre;
    /**
     * @ORM\Column(name="AgeMin",type="integer")
     */
    private $AgeMin;
    /**
     * @ORM\Column(name="AgeMax",type="integer")
     */
    private $AgeMax;
    /**
     * @ORM\Column(name="Prix",type="float")
     */
    private $prix;
    /**
     * @ORM\ManyToOne(targetEntity="usertestBundle\Entity\User")
     * @ORM\Column(type="integer")
     * @ORM\JoinColumn(name="id_assurance" , referencedColumnName="id")
     */
    private $id_assurance;




    public function __construct()
    {

    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @param int $Id
     */
    public function setId($Id)
    {
        $this->Id = $Id;
    }

    /**
     * @return mixed
     */
    public function getTypeOffre()
    {
        return $this->typeOffre;
    }

    /**
     * @param mixed $typeOffre
     */
    public function setTypeOffre($typeOffre)
    {
        $this->typeOffre = $typeOffre;
    }

    /**
     * @return mixed
     */
    public function getDescriptionOffre()
    {
        return $this->descriptionOffre;
    }

    /**
     * @param mixed $descriptionOffre
     */
    public function setDescriptionOffre($descriptionOffre)
    {
        $this->descriptionOffre = $descriptionOffre;
    }

    /**
     * @return mixed
     */
    public function getAgeMin()
    {
        return $this->AgeMin;
    }

    /**
     * @param mixed $AgeMin
     */
    public function setAgeMin($AgeMin)
    {
        $this->AgeMin = $AgeMin;
    }

    /**
     * @return mixed
     */
    public function getAgeMax()
    {
        return $this->AgeMax;
    }

    /**
     * @param mixed $AgeMax
     */
    public function setAgeMax($AgeMax)
    {
        $this->AgeMax = $AgeMax;
    }

    /**
     * @return mixed
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * @param mixed $prix
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;
    }

    /**
     * @return mixed
     */
    public function getIdAssurance()
    {
        return $this->id_assurance;
    }

    /**
     * @param mixed $id_assurance
     */
    public function setIdAssurance($id_assurance)
    {
        $this->id_assurance = $id_assurance;
    }








}