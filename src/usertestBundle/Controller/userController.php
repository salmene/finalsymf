<?php

namespace usertestBundle\Controller;

use OffreBundle\Entity\Offre;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Output\ConsoleOutput;
use usertestBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use usertestBundle\Repository\UserRepository;



class userController extends Controller
{



    public function ajAction(Request $req)
    {
        $user = new User();
//$offre= new Offre();
        if ($req->isMethod("POST")) {
//$offre->setTypeOffre($req->get('login'));
            $user->setEmail($req->get('login'));
            $user->setAgence($req->get('agence'));
            $npn=$req->get('nom')." ".$req->get('prenom');
            $user->setUsername($npn);
           // $user->setPrenom();
            $user->setPassword($req->get('mdp'));

            $user->setDateNaissance($req->get('date'));
            $user->setNumTel($req->get('tel'));
            $user->setRoles(array('Client'));
            $user->setAgence('Star');
           // $user->setType('Normal');
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('Accueil');
        }
        return $this->render('@usertest/Accueil.html.twig', array());
    }

    public function tousagencesAction()
    {

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository("usertestBundle:User")->recherche();
        return $this->render('@usertest/Accueil.html.twig', array('m' => $user));

    }

    public function loginAction(Request $req)
    {
        $user = new User();
        $user->setRoles(array('Client'));

        if ($req->isMethod("POST")  ) {
            $user->setLogin($req->get('login'));
            $user->setPassword($req->get('mdp'));
            $user->setAgence($req->get('agence'));



            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository("usertestBundle:User")->login($user->getLogin(), $user->getPassword(), $user->getAgence(),'CLIENT');
            $em->flush();
            if (!$user) {
                return $this->redirectToRoute('Accueil');
            } else  {
                return $this->redirectToRoute('redirectAccClient');
            }

        }
        return  $this->redirectToRoute('Accueil');
    }

    public function loginAssAction(Request $req)
    {
        $user=new User();
        $user->setRoles(array('Responsable'));

       if($req ->isMethod("POST")) {
           $user->setLogin($req->get('login'));
           $user->setPassword($req->get('mdp'));

           $em=$this->getDoctrine()->getManager();
           $user=$em->getRepository("usertestBundle:User")->loginass($user->getLogin(),$user->getPassword(),'RESPONSABLE');
           $em->flush();

           if (!$user) {
               return  $this->redirectToRoute('Accueil');
           }else
           {
               $session = $req->getSession();


               $session->set('log',$req->get('login'));
               return $this->redirectToRoute('redirectAccAss');

           }

       }


    /*    if($req ->isMethod("POST")){
            $user->setLogin($req->get('login'));
            $user->setPassword($req->get('mdp'));

            $em=$this->getDoctrine()->getManager();
            $user=$em->getRepository("usertestBundle:User")->loginass($user->getLogin(),$user->getPassword());
            $em->flush();
            if (!$user) {
                return  $this->redirectToRoute('Accueil');
            }else
            {
                return $this->redirectToRoute('redirectAccAss');
            }

        }*/

    }

    public  function redirectToAccueilClientAction(){
        return $this->render('@usertest/AccueilClient.html.twig',array());
    }
    public  function redirectToAccueilAssAction(){
        return $this->render('@usertest/AccueilAss.html.twig',array());
    }



}