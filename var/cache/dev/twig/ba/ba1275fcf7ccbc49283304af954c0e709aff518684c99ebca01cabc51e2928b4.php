<?php

/* @Offre/OffresAss.html.twig */
class __TwigTemplate_598b963d88cfc5ca259821782484bdf55ba7fd89c7707c220b1dbfc84179ffb2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("default/index.html.twig", "@Offre/OffresAss.html.twig", 1);
        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "default/index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Offre/OffresAss.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Offre/OffresAss.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_stylesheets($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 3
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
    <link rel=\"stylesheet\" href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/flexslider.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"screen\" property=\"\" />
    <link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
    <link href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/fontawesome-all.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css2/owl.carousel.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
    <link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css2/owl.theme.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
    <link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css2/style2.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 11
    public function block_content($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 12
        echo "    <br><br><br><br><br>
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <h5 class=\"modal-title\" id=\"exampleModalLabel2\">Ajout offre</h5>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span>
                </button>
            </div>
            <div class=\"modal-body\">
                <div class=\"login p-md-5 p-4\">
                    <form action=\"";
        // line 23
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajoutOffre");
        echo "\" method=\"post\">

                           <center><div class=\"form-group col-md-6 mb-3\">
                                <label>Type</label>

                                <input type=\"text\" name =\"type\" class=\"form-control\" id=\"validationDefault01\" placeholder=\"\" required=\"\">
                            </div></center>

                           <center> <div class=\"form-group col-md-6 mb-3\">
                                <label>Description</label>
                                <textarea id=\"story\" name=\"desc\"
                                             rows=\"5\" cols=\"33\">

                                </textarea>
                               </div></center>



                            <center><div class=\"form-group col-md-6\">
                                <label>Prix</label>
                                <input type=\"text\" name=\"prix\" class=\"form-control\" id=\"validationDefault02\" placeholder=\"\" required=\"\">
                            </div></center>
                        <center><div class=\"form-group col-md-6\">
                                <label>Age minimale</label>
                                <select name=\"agemin\" class=\"form-control\">
                                    <option value=\"1\">1</option>
                                    <option value=\"10\">10</option>
                                    <option value=\"15\">15</option>
                                    <option value=\"20\">20</option>
                                    <option value=\"30\">30</option>
                                    <option value=\"40\">40</option>
                                    <option value=\"50\">50</option>
                                    <option value=\"60\">60</option>
                                    <option value=\"70\">70</option>
                                </select>
                            </div></center>
                            <center><div class=\"form-group col-md-6\">
                                <label>Age maximale</label>
                                <select name=\"agemax\" class=\"form-control\">
                                    <option value=\"1\">1</option>
                                    <option value=\"10\">10</option>
                                    <option value=\"15\">15</option>
                                    <option value=\"20\">20</option>
                                    <option value=\"30\">30</option>
                                    <option value=\"40\">40</option>
                                    <option value=\"50\">50</option>
                                    <option value=\"60\">60</option>
                                    <option value=\"70\">70</option>

                                </select>
                            </div></center>


<br><br>
                        <center><button type=\"submit\" class=\"btn btn-primary submit mb-4\">Ajouter l'offre</button></center>

                    </form>

                </div>

            </div>
        </div>
    </div>





    <section class=\"banner-w3layouts-bottom py-lg-5 py-3\">
        <div class=\"container py-lg-4 py-3\">
            <div class=\"inner-sec-agileits-w3ls\">
                <div class=\"owl-carousel owl-theme\">

                    ";
        // line 96
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["Offre"]) ? $context["Offre"] : $this->getContext($context, "Offre")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 97
            echo "                    <div class=\"item\" style=\"width:850px\">
                    <div class=\"lm-item lm-item-2\">
                        <div class=\"lm-item-top\">
                            <span class=\"lm-item-title lm-underline\">";
            // line 100
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "typeOffre", []), "html", null, true);
            echo "</span>
                            <div class=\"lm-item-price\">";
            // line 101
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "prix", []), "html", null, true);
            echo "
                                <span>/ par an</span>
                            </div>

                        </div>
                        <div class=\"lm-item-body\">

                            <ul class=\"lm-item-list\">
                                <li>
                                    <strong>Description</strong> ";
            // line 110
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "descriptionOffre", []), "html", null, true);
            echo "</li>
                                <li>
                                    <strong>Age min</strong> ";
            // line 112
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "AgeMin", []), "html", null, true);
            echo "</li>
                                <li>
                                    <strong>Age max</strong> ";
            // line 114
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "AgeMax", []), "html", null, true);
            echo " </li>
                                <input type=\"text\" name=\"ido\" value=\"";
            // line 115
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "Id", []), "html", null, true);
            echo "\" hidden>

                            </ul>
                            <div class=\"buy-button\">

                                <button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#exampleModal1";
            // line 120
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "Id", []), "html", null, true);
            echo "\">Modifier</button>
                                <button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#exampleModal2";
            // line 121
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "Id", []), "html", null, true);
            echo "\">Supprimer</button>

                            </div>
                        </div>
                        <div class=\"lm-item-bottom\"></div>
                    </div>
                </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 129
        echo "




















                </div>
            </div>
        </div>
    </section>

";
        // line 155
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["Offre"]) ? $context["Offre"] : $this->getContext($context, "Offre")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 156
            echo "
    <div class=\"modal fade\" id=\"exampleModal1";
            // line 157
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "Id", []), "html", null, true);
            echo "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel1\" aria-hidden=\"true\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h5 class=\"modal-title\" id=\"exampleModalLabel1\">";
            // line 161
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "typeOffre", []), "html", null, true);
            echo "</h5>
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                </div>
                <div class=\"modal-body\">
                    <div class=\"login p-md-5 p-4\">
                        <form action=\"#\" method=\"POST\">
                            <div class=\"form-group\">
                                <label>Description</label>
                                <textarea id=\"story\" name=\"story\"
                                          rows=\"5\" cols=\"33\">
";
            // line 173
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "descriptionOffre", []), "html", null, true);
            echo "
                                </textarea>
                            </div>
                            <div class=\"form-group\">
                                <label>Prix</label>
                                <input type=\"text\" value=\"";
            // line 178
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "prix", []), "html", null, true);
            echo "\" name=\"mdp\" class=\"form-control\" id=\"exampleInputPassword1\" placeholder=\"\" required=\"\">
                            </div>
                            <div class=\"form-group\">
                                <label>Age minimale</label>

                                <select class=\"form-control\" name=\"agence\">
                                    <option value=\"";
            // line 184
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "AgeMin", []), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "AgeMin", []), "html", null, true);
            echo "</option>
                                    <option value=\"1\">1</option>
                                    <option value=\"10\">10</option>
                                    <option value=\"15\">15</option>
                                    <option value=\"20\">20</option>
                                    <option value=\"30\">30</option>
                                    <option value=\"40\">40</option>
                                    <option value=\"50\">50</option>
                                    <option value=\"60\">60</option>
                                    <option value=\"70\">70</option>
                                </select>
                            </div>
                            <div class=\"form-group\">
                                <label>Age maximale</label>

                                <select class=\"form-control\" name=\"agence\">
                                    <option value=\"";
            // line 200
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "AgeMax", []), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "AgeMin", []), "html", null, true);
            echo "</option>
                                    <option value=\"1\">1</option>
                                    <option value=\"10\">10</option>
                                    <option value=\"15\">15</option>
                                    <option value=\"20\">20</option>
                                    <option value=\"30\">30</option>
                                    <option value=\"40\">40</option>
                                    <option value=\"50\">50</option>
                                    <option value=\"60\">60</option>
                                    <option value=\"70\">70</option>
                                </select>
                                <input type=\"text\" value=\"";
            // line 211
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "Id", []), "html", null, true);
            echo "\" hidden>
                            </div>

                            <button type=\"submit\" class=\"btn btn-primary submit mb-4\">Modifier</button>

                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>




    <div class=\"modal fade\" id=\"exampleModal2";
            // line 227
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "Id", []), "html", null, true);
            echo "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel1\" aria-hidden=\"true\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h6 class=\"modal-title\" id=\"exampleModalLabel1\">Voulez-vous vraiment supprimer l'offre...?</h6>
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                    <input type=\"text\" value=\"";
            // line 235
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "Id", []), "html", null, true);
            echo "\" hidden>
                </div>
                <div class=\"modal-body\">
                    <div class=\"login p-md-5 p-4\">
                        <form action=\"#\" method=\"POST\">



                            <center><button type=\"submit\" class=\"btn btn-primary submit mb-4\">Supprimer</button></center>

                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 253
        echo "






















    <script src=\"";
        // line 276
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js2/jquery-2.2.3.min.js"), "html", null, true);
        echo "\"></script>
    <!-- carousel -->
    <script src=\"";
        // line 278
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js2/owl.carousel.js"), "html", null, true);
        echo "\"></script>
    <script>
        \$(document).ready(function () {
            \$('.owl-carousel').owlCarousel({
                loop: true,
                margin: 10,
                responsiveClass: true,
                responsive: {
                    0: {
                        items: 1,
                        nav: true
                    },
                    600: {
                        items: 2,
                        nav: false
                    },
                    900: {
                        items: 3,
                        nav: false
                    },
                    1000: {
                        items: 4,
                        nav: true,
                        loop: false,
                        margin: 20
                    }
                }
            })
        })
    </script>
    <!-- //carousel -->
    <!-- stats -->

    <script>
        \$('.counter').countUp();
    </script>
    <!-- //stats -->
    <!-- flexSlider -->

    <script>
        \$(window).load(function () {
            \$('.flexslider').flexslider({
                animation: \"slide\",
                start: function (slider) {
                    \$('body').removeClass('loading');
                }
            });
        });
    </script>
    <!-- //flexSlider -->
    <!-- start-smooth-scrolling -->

    <script>
        jQuery(document).ready(function (\$) {
            \$(\".scroll\").click(function (event) {
                event.preventDefault();

                \$('html,body').animate({
                    scrollTop: \$(this.hash).offset().top
                }, 1000);
            });
        });
    </script>
    <!-- //end-smooth-scrolling -->
    <!-- dropdown nav -->
    <script>
        \$(document).ready(function () {
            \$(\".dropdown\").hover(
                function () {
                    \$('.dropdown-menu', this).stop(true, true).slideDown(\"fast\");
                    \$(this).toggleClass('open');
                },
                function () {
                    \$('.dropdown-menu', this).stop(true, true).slideUp(\"fast\");
                    \$(this).toggleClass('open');
                }
            );
        });
    </script>
    <!-- //dropdown nav -->
    <!-- smooth-scrolling-of-move-up -->
    <script>
        \$(document).ready(function () {
            /*
             var defaults = {
             containerID: 'toTop', // fading element id
             containerHoverID: 'toTopHover', // fading element hover id
             scrollSpeed: 1200,
             easingType: 'linear'
             };
             */

            \$().UItoTop({
                easingType: 'easeOutQuart'
            });

        });
    </script>
    <script src=\"";
        // line 376
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@Offre/OffresAss.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  546 => 376,  445 => 278,  440 => 276,  415 => 253,  391 => 235,  380 => 227,  361 => 211,  345 => 200,  324 => 184,  315 => 178,  307 => 173,  292 => 161,  285 => 157,  282 => 156,  278 => 155,  250 => 129,  236 => 121,  232 => 120,  224 => 115,  220 => 114,  215 => 112,  210 => 110,  198 => 101,  194 => 100,  189 => 97,  185 => 96,  109 => 23,  96 => 12,  87 => 11,  75 => 9,  71 => 8,  67 => 7,  63 => 6,  59 => 5,  55 => 4,  50 => 3,  41 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'default/index.html.twig' %}
{% block stylesheets %}
    <link href=\"{{ asset('css/bootstrap.css') }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
    <link rel=\"stylesheet\" href=\"{{ asset('css/flexslider.css') }}\" type=\"text/css\" media=\"screen\" property=\"\" />
    <link href=\"{{ asset('css/style.css') }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
    <link href=\"{{ asset('css/fontawesome-all.css') }}\" rel=\"stylesheet\">
    <link rel=\"stylesheet\" href=\"{{ asset('css2/owl.carousel.css') }}\" type=\"text/css\" media=\"all\">
    <link rel=\"stylesheet\" href=\"{{ asset('css2/owl.theme.css') }}\" type=\"text/css\" media=\"all\">
    <link rel=\"stylesheet\" href=\"{{ asset('css2/style2.css') }}\" type=\"text/css\" media=\"all\" />
{% endblock %}
{% block content %}
    <br><br><br><br><br>
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <h5 class=\"modal-title\" id=\"exampleModalLabel2\">Ajout offre</h5>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span>
                </button>
            </div>
            <div class=\"modal-body\">
                <div class=\"login p-md-5 p-4\">
                    <form action=\"{{ path('ajoutOffre') }}\" method=\"post\">

                           <center><div class=\"form-group col-md-6 mb-3\">
                                <label>Type</label>

                                <input type=\"text\" name =\"type\" class=\"form-control\" id=\"validationDefault01\" placeholder=\"\" required=\"\">
                            </div></center>

                           <center> <div class=\"form-group col-md-6 mb-3\">
                                <label>Description</label>
                                <textarea id=\"story\" name=\"desc\"
                                             rows=\"5\" cols=\"33\">

                                </textarea>
                               </div></center>



                            <center><div class=\"form-group col-md-6\">
                                <label>Prix</label>
                                <input type=\"text\" name=\"prix\" class=\"form-control\" id=\"validationDefault02\" placeholder=\"\" required=\"\">
                            </div></center>
                        <center><div class=\"form-group col-md-6\">
                                <label>Age minimale</label>
                                <select name=\"agemin\" class=\"form-control\">
                                    <option value=\"1\">1</option>
                                    <option value=\"10\">10</option>
                                    <option value=\"15\">15</option>
                                    <option value=\"20\">20</option>
                                    <option value=\"30\">30</option>
                                    <option value=\"40\">40</option>
                                    <option value=\"50\">50</option>
                                    <option value=\"60\">60</option>
                                    <option value=\"70\">70</option>
                                </select>
                            </div></center>
                            <center><div class=\"form-group col-md-6\">
                                <label>Age maximale</label>
                                <select name=\"agemax\" class=\"form-control\">
                                    <option value=\"1\">1</option>
                                    <option value=\"10\">10</option>
                                    <option value=\"15\">15</option>
                                    <option value=\"20\">20</option>
                                    <option value=\"30\">30</option>
                                    <option value=\"40\">40</option>
                                    <option value=\"50\">50</option>
                                    <option value=\"60\">60</option>
                                    <option value=\"70\">70</option>

                                </select>
                            </div></center>


<br><br>
                        <center><button type=\"submit\" class=\"btn btn-primary submit mb-4\">Ajouter l'offre</button></center>

                    </form>

                </div>

            </div>
        </div>
    </div>





    <section class=\"banner-w3layouts-bottom py-lg-5 py-3\">
        <div class=\"container py-lg-4 py-3\">
            <div class=\"inner-sec-agileits-w3ls\">
                <div class=\"owl-carousel owl-theme\">

                    {% for i in Offre  %}
                    <div class=\"item\" style=\"width:850px\">
                    <div class=\"lm-item lm-item-2\">
                        <div class=\"lm-item-top\">
                            <span class=\"lm-item-title lm-underline\">{{ i.typeOffre }}</span>
                            <div class=\"lm-item-price\">{{ i.prix }}
                                <span>/ par an</span>
                            </div>

                        </div>
                        <div class=\"lm-item-body\">

                            <ul class=\"lm-item-list\">
                                <li>
                                    <strong>Description</strong> {{ i.descriptionOffre }}</li>
                                <li>
                                    <strong>Age min</strong> {{ i.AgeMin }}</li>
                                <li>
                                    <strong>Age max</strong> {{ i.AgeMax }} </li>
                                <input type=\"text\" name=\"ido\" value=\"{{ i.Id }}\" hidden>

                            </ul>
                            <div class=\"buy-button\">

                                <button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#exampleModal1{{ i.Id }}\">Modifier</button>
                                <button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#exampleModal2{{ i.Id }}\">Supprimer</button>

                            </div>
                        </div>
                        <div class=\"lm-item-bottom\"></div>
                    </div>
                </div>
                    {% endfor %}





















                </div>
            </div>
        </div>
    </section>

{% for i in Offre %}

    <div class=\"modal fade\" id=\"exampleModal1{{ i.Id }}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel1\" aria-hidden=\"true\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h5 class=\"modal-title\" id=\"exampleModalLabel1\">{{ i.typeOffre }}</h5>
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                </div>
                <div class=\"modal-body\">
                    <div class=\"login p-md-5 p-4\">
                        <form action=\"#\" method=\"POST\">
                            <div class=\"form-group\">
                                <label>Description</label>
                                <textarea id=\"story\" name=\"story\"
                                          rows=\"5\" cols=\"33\">
{{ i.descriptionOffre }}
                                </textarea>
                            </div>
                            <div class=\"form-group\">
                                <label>Prix</label>
                                <input type=\"text\" value=\"{{ i.prix }}\" name=\"mdp\" class=\"form-control\" id=\"exampleInputPassword1\" placeholder=\"\" required=\"\">
                            </div>
                            <div class=\"form-group\">
                                <label>Age minimale</label>

                                <select class=\"form-control\" name=\"agence\">
                                    <option value=\"{{ i.AgeMin }}\">{{ i.AgeMin }}</option>
                                    <option value=\"1\">1</option>
                                    <option value=\"10\">10</option>
                                    <option value=\"15\">15</option>
                                    <option value=\"20\">20</option>
                                    <option value=\"30\">30</option>
                                    <option value=\"40\">40</option>
                                    <option value=\"50\">50</option>
                                    <option value=\"60\">60</option>
                                    <option value=\"70\">70</option>
                                </select>
                            </div>
                            <div class=\"form-group\">
                                <label>Age maximale</label>

                                <select class=\"form-control\" name=\"agence\">
                                    <option value=\"{{ i.AgeMax }}\">{{ i.AgeMin }}</option>
                                    <option value=\"1\">1</option>
                                    <option value=\"10\">10</option>
                                    <option value=\"15\">15</option>
                                    <option value=\"20\">20</option>
                                    <option value=\"30\">30</option>
                                    <option value=\"40\">40</option>
                                    <option value=\"50\">50</option>
                                    <option value=\"60\">60</option>
                                    <option value=\"70\">70</option>
                                </select>
                                <input type=\"text\" value=\"{{ i.Id }}\" hidden>
                            </div>

                            <button type=\"submit\" class=\"btn btn-primary submit mb-4\">Modifier</button>

                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>




    <div class=\"modal fade\" id=\"exampleModal2{{ i.Id }}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel1\" aria-hidden=\"true\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h6 class=\"modal-title\" id=\"exampleModalLabel1\">Voulez-vous vraiment supprimer l'offre...?</h6>
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                    <input type=\"text\" value=\"{{ i.Id }}\" hidden>
                </div>
                <div class=\"modal-body\">
                    <div class=\"login p-md-5 p-4\">
                        <form action=\"#\" method=\"POST\">



                            <center><button type=\"submit\" class=\"btn btn-primary submit mb-4\">Supprimer</button></center>

                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
{% endfor %}























    <script src=\"{{ asset('js2/jquery-2.2.3.min.js') }}\"></script>
    <!-- carousel -->
    <script src=\"{{ asset('js2/owl.carousel.js') }}\"></script>
    <script>
        \$(document).ready(function () {
            \$('.owl-carousel').owlCarousel({
                loop: true,
                margin: 10,
                responsiveClass: true,
                responsive: {
                    0: {
                        items: 1,
                        nav: true
                    },
                    600: {
                        items: 2,
                        nav: false
                    },
                    900: {
                        items: 3,
                        nav: false
                    },
                    1000: {
                        items: 4,
                        nav: true,
                        loop: false,
                        margin: 20
                    }
                }
            })
        })
    </script>
    <!-- //carousel -->
    <!-- stats -->

    <script>
        \$('.counter').countUp();
    </script>
    <!-- //stats -->
    <!-- flexSlider -->

    <script>
        \$(window).load(function () {
            \$('.flexslider').flexslider({
                animation: \"slide\",
                start: function (slider) {
                    \$('body').removeClass('loading');
                }
            });
        });
    </script>
    <!-- //flexSlider -->
    <!-- start-smooth-scrolling -->

    <script>
        jQuery(document).ready(function (\$) {
            \$(\".scroll\").click(function (event) {
                event.preventDefault();

                \$('html,body').animate({
                    scrollTop: \$(this.hash).offset().top
                }, 1000);
            });
        });
    </script>
    <!-- //end-smooth-scrolling -->
    <!-- dropdown nav -->
    <script>
        \$(document).ready(function () {
            \$(\".dropdown\").hover(
                function () {
                    \$('.dropdown-menu', this).stop(true, true).slideDown(\"fast\");
                    \$(this).toggleClass('open');
                },
                function () {
                    \$('.dropdown-menu', this).stop(true, true).slideUp(\"fast\");
                    \$(this).toggleClass('open');
                }
            );
        });
    </script>
    <!-- //dropdown nav -->
    <!-- smooth-scrolling-of-move-up -->
    <script>
        \$(document).ready(function () {
            /*
             var defaults = {
             containerID: 'toTop', // fading element id
             containerHoverID: 'toTopHover', // fading element hover id
             scrollSpeed: 1200,
             easingType: 'linear'
             };
             */

            \$().UItoTop({
                easingType: 'easeOutQuart'
            });

        });
    </script>
    <script src=\"{{ asset('js/bootstrap.min.js') }}\"></script>
{% endblock %}

", "@Offre/OffresAss.html.twig", "C:\\wamp64\\www\\untitled\\src\\OffreBundle\\Resources\\views\\OffresAss.html.twig");
    }
}
