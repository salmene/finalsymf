<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 30/03/2019
 * Time: 11:43
 */
namespace usertestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Output\ConsoleOutput;
use usertestBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use OffreBundle\Repository\OffreRepository;

class acceuilAssController extends Controller{

    public function redirecTtoOffreAction(Request $req)
    {
        $em=$this->getDoctrine()->getManager();
        $session = $req->getSession();
       // $r=$session->get('log');
        $id=$em->getRepository("usertestBundle:User")->getIdByLogin($session->get('log'));
        settype($id,"integer");
        $offre=$em->getRepository("OffreBundle:Offre")->rechercheOffAss($id);
        return $this->render('@Offre/OffresAss.html.twig',array('Offre'=>$offre));
    }



}