<?php
namespace OffreBundle\Repository;
use Doctrine\ORM\EntityRepository;

class OffreRepository extends EntityRepository{


    public function rechercheOffAss($r){
        $req=$this->getEntityManager()
            ->createQuery("select o from OffreBundle:Offre o where o.id_assurance=:r")
            ->setParameter('r',$r);
        return $req->getResult();
    }



}
