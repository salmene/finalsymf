<?php

/* @Offre/Offres.html.twig */
class __TwigTemplate_a8ffb8265e2a13dc5b92e4db366c08d516ab7a3ff677f64b15bf48ea8cf71986 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "@Offre/Offres.html.twig", 1);
        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Offre/Offres.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Offre/Offres.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_content($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 3
        echo "
    <br><br><br><br>

        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h5 class=\"modal-title\" id=\"exampleModalLabel2\">Register</h5>
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                </div>
                <div class=\"modal-body\">
                    <div class=\"login p-md-5 p-4\">
                        <form action=\"#\" method=\"post\">
                            <div class=\"form-row\">
                                <div class=\"form-group col-md-6 mb-3\">
                                    <label>First name</label>

                                    <input type=\"text\" class=\"form-control\" id=\"validationDefault01\" placeholder=\"\" required=\"\">
                                </div>
                                <div class=\"form-group col-md-6 mb-3\">
                                    <label>Last name</label>
                                    <input type=\"text\" class=\"form-control\" id=\"validationDefault02\" placeholder=\"\" required=\"\">
                                </div>
                            </div>
                            <div class=\"form-row\">
                                <div class=\"form-group col-md-6\">
                                    <label>Password</label>
                                    <input type=\"password\" class=\"form-control\" id=\"password1\" placeholder=\"\" required=\"\">
                                </div>
                                <div class=\"form-group col-md-6\">
                                    <label>Confirm Password</label>
                                    <input type=\"password\" class=\"form-control\" id=\"password2\" placeholder=\"\" required=\"\">
                                </div>

                            </div>
                            <button type=\"submit\" class=\"btn btn-primary submit mb-4\">Register</button>
                            <p>
                                <a href=\"#\" class=\"modal-btm-text\">By clicking Register, I agree to your terms</a>
                            </p>
                        </form>

                    </div>

                </div>
            </div>
        </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@Offre/Offres.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 3,  40 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}
{% block content %}

    <br><br><br><br>

        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h5 class=\"modal-title\" id=\"exampleModalLabel2\">Register</h5>
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                </div>
                <div class=\"modal-body\">
                    <div class=\"login p-md-5 p-4\">
                        <form action=\"#\" method=\"post\">
                            <div class=\"form-row\">
                                <div class=\"form-group col-md-6 mb-3\">
                                    <label>First name</label>

                                    <input type=\"text\" class=\"form-control\" id=\"validationDefault01\" placeholder=\"\" required=\"\">
                                </div>
                                <div class=\"form-group col-md-6 mb-3\">
                                    <label>Last name</label>
                                    <input type=\"text\" class=\"form-control\" id=\"validationDefault02\" placeholder=\"\" required=\"\">
                                </div>
                            </div>
                            <div class=\"form-row\">
                                <div class=\"form-group col-md-6\">
                                    <label>Password</label>
                                    <input type=\"password\" class=\"form-control\" id=\"password1\" placeholder=\"\" required=\"\">
                                </div>
                                <div class=\"form-group col-md-6\">
                                    <label>Confirm Password</label>
                                    <input type=\"password\" class=\"form-control\" id=\"password2\" placeholder=\"\" required=\"\">
                                </div>

                            </div>
                            <button type=\"submit\" class=\"btn btn-primary submit mb-4\">Register</button>
                            <p>
                                <a href=\"#\" class=\"modal-btm-text\">By clicking Register, I agree to your terms</a>
                            </p>
                        </form>

                    </div>

                </div>
            </div>
        </div>

{% endblock %}", "@Offre/Offres.html.twig", "C:\\wamp64\\www\\untitled\\src\\OffreBundle\\Resources\\views\\Offres.html.twig");
    }
}
