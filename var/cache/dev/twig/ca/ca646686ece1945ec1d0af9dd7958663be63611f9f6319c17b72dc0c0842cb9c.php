<?php

/* @usertest/Accueil.html.twig */
class __TwigTemplate_b2b7b836142c335327c97e0687f1a6a7fbc8aa844c5f4b6c8c747daacc97e967 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@usertest/Accueil.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@usertest/Accueil.html.twig"));

        // line 1
        echo "<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang=\"en\">

<head>
    <title>Brisk Biz Corporate Category Bootstrap Responsive Website Template | Home :: w3layouts</title>
    <!-- Meta Tags -->
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <meta charset=\"utf-8\">
    <meta name=\"keywords\" content=\"Brisk Biz Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design\" />
    <script>
        addEventListener(\"load\", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- //Meta Tags -->
    <!-- Style-sheets -->
    <link href= \"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
    <link rel=\"stylesheet\" href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/flexslider.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"screen\" property=\"\" />
    <link href=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
    <link href=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/fontawesome-all.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <!--// Style-sheets -->

</head>

<body>

<!-- banner -->
<section class=\"top-agileits-w3layouts container-fluid p-3\">
    <div class=\"row\">

        <h1 class=\"col-md-4 logo order-md-2 order-1 text-center\">
            <a class=\"navbar-brand\" href=\"index.html\">
                <i class=\"fab fa-keycdn\"></i>Brisk Biz</a>
        </h1>

        <div class=\"col-md-4 order-md-1 order-2 text-left align-self-center top-mid-w3l\">
            <!-- Login Button -->
            <button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#exampleModal1\">
                Client?
            </button>
            <button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#exampleModal\">
                Assurance?
            </button>
            <!--// Login Button -->
            <!-- Register Button -->
            <button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#exampleModal2\">
                Pas de compte?
            </button>
            <!--// Register Button -->
        </div>
        <div class=\"col-md-4 order-3 log-icons text-right align-self-center\">

            <ul class=\"social_list_wthree\">

                <li class=\"text-center\">
                    <a href=\"#\" class=\"facebook1\">
                        <i class=\"fab fa-facebook-f\"></i>

                    </a>
                </li>
                <li class=\"text-center mx-3\">
                    <a href=\"#\" class=\"twitter2\">
                        <i class=\"fab fa-twitter\"></i>

                    </a>
                </li>
                <li class=\"text-center\">
                    <a href=\"#\" class=\"dribble3\">
                        <i class=\"fab fa-dribbble\"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</section>
<section class=\"banner\" id=\"home\">

    <!-- banner-text -->
    <div id=\"carouselExampleControls\" class=\"carousel slide\" data-ride=\"carousel\">
        <div class=\"carousel-inner text-right\">
            <div class=\"carousel-item b1 active\">
                <div class=\"container banner-info-w3-agile\">
                    <h3>Services to Grow Your Business</h3>
                    <p class=\"mt-lg-4 mt-md-3 mt-2 p-2 pl-4\">Creativity & Experience</p>
                </div>
            </div>
            <div class=\"carousel-item b2\">
                <div class=\"container banner-info-w3-agile\">
                    <h3>Various Options for Your Benefit</h3>
                    <p class=\"mt-lg-4 mt-md-3 mt-2 p-2 pl-4\">Creativity & Experience</p>
                </div>
            </div>
            <div class=\"carousel-item b3\">
                <div class=\"container banner-info-w3-agile\">
                    <h3>Enhancing Your Brand’s Appeal</h3>
                    <p class=\"mt-lg-4 mt-md-3 mt-2 p-2 pl-4\">Creativity & Experience</p>
                </div>
            </div>
            <div class=\"carousel-item b4\">
                <div class=\"container banner-info-w3-agile\">
                    <h3>Converting Concepts into Reality</h3>
                    <p class=\"mt-lg-4 mt-md-3 mt-2 p-2 pl-4\">Creativity & Experience</p>
                </div>
            </div>
        </div>
        <a class=\"carousel-control-prev\" href=\"#carouselExampleControls\" role=\"button\" data-slide=\"prev\">
            <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
            <span class=\"sr-only\">Previous</span>
        </a>
        <a class=\"carousel-control-next\" href=\"#carouselExampleControls\" role=\"button\" data-slide=\"next\">
            <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
            <span class=\"sr-only\">Next</span>
        </a>
    </div>
</section>
<!-- //banner -->
<!--about-->
<section class=\"banner-btm-section py-5\" id=\"about\">
    <div class=\"container py-xl-5 py-sm-3\">
        <h5 class=\"main-w3l-title mb-sm-4 mb-3\">Our Strategy</h5>
        <div class=\"srategy-text mb-5\">
            <p class=\"paragraph-agileinfo\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer quis tristique est, et egestas odio. Mauris ac tristique
                arcu, sed interdum risus.Integer quis tristique est, et egestas odio. Mauris ac tristique arcu, sed interdum risus.
            </p>
        </div>
        <div class=\"bnr-btm-main row justify-content-around\">
            <div class=\"col-lg-4 banner-btm-grids\">
                <div class=\"bnr-btm-info bb1 p-4\">
                    <i class=\"fab fa-gg text-white mb-3 ml-3\"></i>
                    <h3 class=\"subheading-wthree mb-md-4 mb-3\">Personal Branding</h3>
                    <p class=\"paragraph-agileinfo text-white\">Maecenas interdum, metus vitae tincidunt porttitor, magna quam egestas sem, ac scelerisque nisl nibh vel lacus.</p>
                    <span>1</span>
                </div>
            </div>
            <div class=\"col-lg-4 banner-btm-grids my-lg-0 my-4\">
                <div class=\"bnr-btm-info bb2 p-4\">
                    <i class=\"fab fa-hubspot text-white mb-3 ml-3\"></i>
                    <h3 class=\"subheading-wthree mb-md-4 mb-3\">Business Consulting</h3>
                    <p class=\"paragraph-agileinfo text-white\">Maecenas interdum, metus vitae tincidunt porttitor, magna quam egestas sem, ac scelerisque nisl nibh vel lacus.</p>
                    <span>2</span>
                </div>
            </div>
            <div class=\"col-lg-4 banner-btm-grids\">
                <div class=\"bnr-btm-info bb3 p-4\">
                    <i class=\"fab fa-connectdevelop text-white mb-3 ml-3\"></i>
                    <h3 class=\"subheading-wthree mb-md-4 mb-3\">Online Marketing</h3>
                    <p class=\"paragraph-agileinfo text-white\">Maecenas interdum, metus vitae tincidunt porttitor, magna quam egestas sem, ac scelerisque nisl nibh vel lacus.</p>
                    <span>3</span>
                </div>
            </div>
        </div>
    </div>
</section>
<!--//about-->
<!-- stats -->
<section class=\"stats py-5\">
    <div class=\"container py-xl-5 py-sm-3\">
        <h5 class=\"main-w3l-title mb-sm-4 mb-3 text-white\">Our Stats</h5>
        <div class=\"row stats_inner\">
            <div class=\"col-lg-3 col-sm-6 stat-grids\">
                <i class=\"fas fa-chart-line\"></i>
                <p class=\"counter\">2,879</p>
                <div class=\"stats-text\">
                    <h3 class=\"text-white\">Projects Done</h3>
                </div>
            </div>
            <div class=\"col-lg-3 col-sm-6 stat-grids mt-sm-0 mt-4\">
                <i class=\"fas fa-user\"></i>
                <p class=\"counter\">350</p>
                <div class=\"stats-text\">
                    <h3 class=\"text-white\">Team</h3>
                </div>
            </div>
            <div class=\"col-lg-3 col-sm-6 stat-grids mt-lg-0 mt-4\">
                <i class=\"fas fa-thumbs-up\"></i>
                <p class=\"counter\">1995</p>
                <div class=\"stats-text\">
                    <h3 class=\"text-white\">Excellent works</h3>
                </div>
            </div>
            <div class=\"col-lg-3 col-sm-6 stat-grids mt-lg-0 mt-4\">
                <i class=\"fas fa-users\"></i>
                <p class=\"counter\">1,546</p>
                <div class=\"stats-text\">
                    <h3 class=\"text-white\">Clients</h3>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- //stats -->
<!-- services -->
<section class=\"News-section py-5\">
    <div class=\"container py-xl-5 py-sm-3\">
        <h5 class=\"main-w3l-title mb-sm-4 mb-3\">Dernières offres</h5>
        <div class=\"row\">
            <div class=\"col-md-4 w3_agile_services_grid\">
                <div class=\"agile_services_grid\">
                    <div class=\"hover06 column\">
                        <div>
                            <a href=\"single.html\">
                                <img src=\"";
        // line 213
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/h1.jpg"), "html", null, true);
        echo "\" class=\"img-fluid\" alt=\"Responsive image\">
                            </a>
                        </div>
                    </div>
                    <div class=\"agile_services_grid_pos\">
                        <span class=\"py-2 px-3\">06 June</span>
                    </div>
                </div>
                <h4 class=\"mt-3 mb-2\">
                    <a href=\"single.html\">Lorem ipsum dolor sit amet.</a>
                </h4>
                <p class=\"paragraph-agileinfo\">Suspendisse dapibus felis mauris, id efficitur lacus tincidunt id.</p>
                <a class=\"btn btn-primary mt-3\" href=\"single.html\" role=\"button\">Read More</a>
            </div>
            <div class=\"col-md-4 w3_agile_services_grid my-md-0 my-4\">
                <div class=\"agile_services_grid\">
                    <div class=\"hover06 column\">
                        <div>
                            <a href=\"single.html\">
                                <img src=\"";
        // line 232
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/h3.jpg"), "html", null, true);
        echo "\" class=\"img-fluid\" alt=\"Responsive image\">
                            </a>
                        </div>
                    </div>
                    <div class=\"agile_services_grid_pos\">
                        <span class=\"py-2 px-3\">23 may</span>
                    </div>
                </div>
                <h4 class=\"mt-3 mb-2\">
                    <a href=\"single.html\">Lorem ipsum dolor sit amet.</a>
                </h4>
                <p class=\"paragraph-agileinfo\">Suspendisse dapibus felis mauris, id efficitur lacus tincidunt id.</p>
                <a class=\"btn btn-primary mt-3\" href=\"single.html\" role=\"button\">Read More</a>
            </div>
            <div class=\"col-md-4 w3_agile_services_grid\">
                <div class=\"agile_services_grid\">
                    <div class=\"hover06 column\">
                        <div>
                            <a href=\"single.html\">
                                <img src=\"";
        // line 251
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/h6.jpg"), "html", null, true);
        echo "\" class=\"img-fluid\" alt=\"Responsive image\">
                            </a>
                        </div>
                    </div>
                    <div class=\"agile_services_grid_pos\">
                        <span class=\"py-2 px-3\">10 may</span>
                    </div>
                </div>
                <h4 class=\"mt-3 mb-2\">
                    <a href=\"single.html\">Lorem ipsum dolor sit amet.</a>
                </h4>
                <p class=\"paragraph-agileinfo\">Suspendisse dapibus felis mauris, id efficitur lacus tincidunt id.</p>
                <a class=\"btn btn-primary mt-3\" href=\"single.html\" role=\"button\">Read More</a>
            </div>
            <div class=\"col-md-6 w3_agile_services_grid mt-md-5 mt-4\">
                <div class=\"agile_services_grid\">
                    <div class=\"hover06 column\">
                        <div>
                            <a href=\"single.html\">
                                <img src=\"";
        // line 270
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/h4.jpg"), "html", null, true);
        echo "\" class=\"img-fluid\" alt=\"Responsive image\">
                            </a>
                        </div>
                    </div>
                    <div class=\"agile_services_grid_pos\">
                        <span class=\"py-2 px-3\">30 April</span>
                    </div>
                </div>
                <h4 class=\"mt-3 mb-2\">
                    <a href=\"single.html\">Lorem ipsum dolor sit amet.</a>
                </h4>
                <p class=\"paragraph-agileinfo\">Duis libero mi, accumsan eu elementum in, tempus quis tortor. Mauris a iaculis dolor. Nunc sagittis, metus vitae blandit
                    fermentum
                </p>
                <a class=\"btn btn-primary mt-3\" href=\"single.html\" role=\"button\">Read More</a>
            </div>
            <div class=\"col-md-6 w3_agile_services_grid mt-md-5 mt-4\">
                <div class=\"agile_services_grid\">
                    <div class=\"hover06 column\">
                        <div>
                            <a href=\"single.html\">
                                <img src=\"";
        // line 291
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/h7.jpg"), "html", null, true);
        echo "\" class=\"img-fluid\" alt=\"Responsive image\">
                            </a>
                        </div>
                    </div>
                    <div class=\"agile_services_grid_pos\">
                        <span class=\"py-2 px-3\">21 April</span>
                    </div>
                </div>
                <h4 class=\"mt-3 mb-2\">
                    <a href=\"single.html\">Lorem ipsum dolor sit amet.</a>
                </h4>
                <p class=\"paragraph-agileinfo\">Duis libero mi, accumsan eu elementum in, tempus quis tortor. Mauris a iaculis dolor. Nunc sagittis, metus vitae blandit
                    fermentum
                </p>
                <a class=\"btn btn-primary mt-3\" href=\"single.html\" role=\"button\">Read More</a>
            </div>

        </div>
    </div>
</section>
<!-- //services -->



<!-- Footer -->

<!-- //Footer -->
<!-- copyright -->
<div class=\"copyright-w3layouts\">
    <div class=\"container\">
        <p class=\"py-xl-4 py-3\">© 2018 Brisk Biz . All Rights Reserved | Design by
            <a href=\"http://w3layouts.com/\"> W3layouts </a>
        </p>
    </div>
</div>
<!-- //copyright -->
<!-- Login Responsable Modal -->
<div class=\"modal fade\" id=\"exampleModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel1\" aria-hidden=\"true\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <h5 class=\"modal-title\" id=\"exampleModalLabel1\">Login</h5>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span>
                </button>
            </div>
            <div class=\"modal-body\">
                <div class=\"login p-md-5 p-4\">
                    <form action=\"";
        // line 339
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("loginAssurance");
        echo "\" method=\"POST\">
                        <div class=\"form-group\">
                            <label>Login</label>
                            <input type=\"email\" name=\"login\" class=\"form-control\" id=\"exampleInputEmail1\" aria-describedby=\"emailHelp\" placeholder=\"\" required=\"\">
                            <small id=\"emailHelp\" class=\"form-text text-muted\">Nous ne partagerons jamais votre email avec qui que ce soit.</small>
                        </div>
                        <div class=\"form-group\">
                            <label>Mot de passe</label>
                            <input type=\"password\" name=\"mdp\" class=\"form-control\" id=\"exampleInputPassword1\" placeholder=\"\" required=\"\">
                        </div>


                        <button type=\"submit\" class=\"btn btn-primary submit mb-4\">Se connecter</button>

                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
<!--// Login Responsable Modal -->
<!-- Login Modal -->
<div class=\"modal fade\" id=\"exampleModal1\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel1\" aria-hidden=\"true\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <h5 class=\"modal-title\" id=\"exampleModalLabel1\">Login</h5>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span>
                </button>
            </div>
            <div class=\"modal-body\">
                <div class=\"login p-md-5 p-4\">
                    <form action=\"";
        // line 373
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("login");
        echo "\" method=\"POST\">
                        <div class=\"form-group\">
                            <label>Login</label>
                            <input type=\"email\" name=\"login\" class=\"form-control\" id=\"exampleInputEmail1\" aria-describedby=\"emailHelp\" placeholder=\"\" required=\"\">
                            <small id=\"emailHelp\" class=\"form-text text-muted\">Nous ne partagerons jamais votre email avec qui que ce soit.</small>
                        </div>
                        <div class=\"form-group\">
                            <label>Mot de passe</label>
                            <input type=\"password\" name=\"mdp\" class=\"form-control\" id=\"exampleInputPassword1\" placeholder=\"\" required=\"\">
                        </div>
                        <div class=\"form-group\">
                            <label>Agence</label>

                            <select class=\"form-control\" name=\"agence\">
                                ";
        // line 387
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 388
            echo "                                    <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "agence", []), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "agence", []), "html", null, true);
            echo "</option>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 390
        echo "                            </select>
                        </div>

                        <button type=\"submit\" class=\"btn btn-primary submit mb-4\">Se connecter</button>

                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
<!--// Login Modal -->
<!-- Register Modal -->
<div class=\"modal fade\" id=\"exampleModal2\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel2\" aria-hidden=\"true\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <h5 class=\"modal-title\" id=\"exampleModalLabel2\">Inscription</h5>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span>
                </button>
            </div>
            <div class=\"modal-body\">
                <div class=\"login p-md-5 p-4\">
                    <form action=\"";
        // line 415
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("inscrire");
        echo "\" method=\"POST\">
                        <div class=\"form-row\">
                            <div class=\"form-group col-md-6 mb-3\">
                                <label>Login</label>

                                <input type=\"text\" name=\"login\" class=\"form-control\" id=\"validationDefault01\" placeholder=\"\" required=\"\">
                            </div>
                            <div class=\"form-group col-md-6 mb-3\">
                                <label>agence</label>

                                <select name=\"agence\" class=\"form-control\">
                                    ";
        // line 426
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 427
            echo "                                        <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "agence", []), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "agence", []), "html", null, true);
            echo "</option>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 429
        echo "                                </select>
                            </div>
                        </div>
                        <div class=\"form-row\">
                            <div class=\"form-group col-md-6 mb-3\">
                                <label>Nom</label>

                                <input type=\"text\"  name=\"nom\" class=\"form-control\" id=\"validationDefault01\" placeholder=\"\" required=\"\">
                            </div>


                            <div class=\"form-group col-md-6 mb-3\">
                                <label>Prénom</label>

                                <input type=\"text\" name=\"prenom\" class=\"form-control\" id=\"validationDefault01\" placeholder=\"\" required=\"\">
                            </div>
                        </div>
                        <div class=\"form-row\">
                            <div class=\"form-group col-md-6 mb-3\">
                                <label>Mot de passe</label>

                                <input type=\"password\" name=\"mdp\" class=\"form-control\" id=\"validationDefault01\" placeholder=\"\" required=\"\">
                            </div>


                            <div class=\"form-group col-md-6 mb-3\">
                                <label>Confirmer le mot de passe</label>

                                <input type=\"password\" name=\"cmdp\" class=\"form-control\" id=\"validationDefault01\" placeholder=\"\" required=\"\">
                            </div>
                        </div>
                        <div class=\"form-row\">
                            <div class=\"form-group col-md-6 mb-3\">
                                <label>Date de naissance</label>

                                <input type=\"date\" name=\"date\" class=\"form-control\" id=\"validationDefault01\" placeholder=\"\" required=\"\">
                            </div>


                            <div class=\"form-group col-md-6 mb-3\">
                                <label>Téléphone</label>

                                <input type=\"text\" name=\"tel\" class=\"form-control\" id=\"validationDefault01\" placeholder=\"\" required=\"\">
                            </div>
                        </div>




                        <button type=\"submit\" class=\"btn btn-primary submit mb-4\">S'inscrire</button>

                    </form>

                </div>

            </div>
        </div>
    </div>
</div>
<!--// Register Modal -->
<!-- Required common Js -->
<script src=\"";
        // line 490
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-2.2.3.min.js"), "html", null, true);
        echo "\"></script>
<!-- //Required common Js -->
<!-- stats -->
<script src=\"";
        // line 493
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery.waypoints.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 494
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery.countup.js"), "html", null, true);
        echo "\"></script>
<script>
    \$('.counter').countUp();
</script>
<!-- //stats -->
<!-- flexSlider -->
<script defer src=\"";
        // line 500
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery.flexslider.js"), "html", null, true);
        echo "\"></script>
<script>
    \$(window).load(function () {
        \$('.flexslider').flexslider({
            animation: \"slide\",
            start: function (slider) {
                \$('body').removeClass('loading');
            }
        });
    });
</script>

<!-- //flexSlider -->

<!-- password-script -->
<script>
    window.onload = function () {
        document.getElementById(\"password1\").onchange = validatePassword;
        document.getElementById(\"password2\").onchange = validatePassword;
    }

    function validatePassword() {
        var pass2 = document.getElementById(\"password2\").value;
        var pass1 = document.getElementById(\"password1\").value;
        if (pass1 != pass2)
            document.getElementById(\"password2\").setCustomValidity(\"Passwords Don't Match\");
        else
            document.getElementById(\"password2\").setCustomValidity('');
        //empty string means no validation error
    }
</script>
<!-- //password-script -->

<!-- start-smoth-scrolling -->
<script src=\"";
        // line 534
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/move-top.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 535
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/easing.js"), "html", null, true);
        echo "\"></script>
<script>
    jQuery(document).ready(function (\$) {
        \$(\".scroll\").click(function (event) {
            event.preventDefault();
            \$('html,body').animate({
                scrollTop: \$(this.hash).offset().top
            }, 1000);
        });
    });
</script>
<!-- start-smoth-scrolling -->
<!-- here stars scrolling icon -->
<script>
    \$(document).ready(function () {
        /*
         var defaults = {
         containerID: 'toTop', // fading element id
         containerHoverID: 'toTopHover', // fading element hover id
         scrollSpeed: 1200,
         easingType: 'linear'
         };
         */

        \$().UItoTop({
            easingType: 'easeOutQuart'
        });

    });
</script>
<!-- //here ends scrolling icon -->
<!-- Js for bootstrap working-->
<script src=\"";
        // line 567
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
<!-- //Js for bootstrap working -->
</body>

</html>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@usertest/Accueil.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  675 => 567,  640 => 535,  636 => 534,  599 => 500,  590 => 494,  586 => 493,  580 => 490,  517 => 429,  506 => 427,  502 => 426,  488 => 415,  461 => 390,  450 => 388,  446 => 387,  429 => 373,  392 => 339,  341 => 291,  317 => 270,  295 => 251,  273 => 232,  251 => 213,  66 => 31,  62 => 30,  58 => 29,  54 => 28,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang=\"en\">

<head>
    <title>Brisk Biz Corporate Category Bootstrap Responsive Website Template | Home :: w3layouts</title>
    <!-- Meta Tags -->
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <meta charset=\"utf-8\">
    <meta name=\"keywords\" content=\"Brisk Biz Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design\" />
    <script>
        addEventListener(\"load\", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- //Meta Tags -->
    <!-- Style-sheets -->
    <link href= \"{{ asset('css/bootstrap.css') }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
    <link rel=\"stylesheet\" href=\"{{ asset('css/flexslider.css') }}\" type=\"text/css\" media=\"screen\" property=\"\" />
    <link href=\"{{ asset('css/style.css') }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
    <link href=\"{{ asset('css/fontawesome-all.css') }}\" rel=\"stylesheet\">
    <!--// Style-sheets -->

</head>

<body>

<!-- banner -->
<section class=\"top-agileits-w3layouts container-fluid p-3\">
    <div class=\"row\">

        <h1 class=\"col-md-4 logo order-md-2 order-1 text-center\">
            <a class=\"navbar-brand\" href=\"index.html\">
                <i class=\"fab fa-keycdn\"></i>Brisk Biz</a>
        </h1>

        <div class=\"col-md-4 order-md-1 order-2 text-left align-self-center top-mid-w3l\">
            <!-- Login Button -->
            <button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#exampleModal1\">
                Client?
            </button>
            <button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#exampleModal\">
                Assurance?
            </button>
            <!--// Login Button -->
            <!-- Register Button -->
            <button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#exampleModal2\">
                Pas de compte?
            </button>
            <!--// Register Button -->
        </div>
        <div class=\"col-md-4 order-3 log-icons text-right align-self-center\">

            <ul class=\"social_list_wthree\">

                <li class=\"text-center\">
                    <a href=\"#\" class=\"facebook1\">
                        <i class=\"fab fa-facebook-f\"></i>

                    </a>
                </li>
                <li class=\"text-center mx-3\">
                    <a href=\"#\" class=\"twitter2\">
                        <i class=\"fab fa-twitter\"></i>

                    </a>
                </li>
                <li class=\"text-center\">
                    <a href=\"#\" class=\"dribble3\">
                        <i class=\"fab fa-dribbble\"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</section>
<section class=\"banner\" id=\"home\">

    <!-- banner-text -->
    <div id=\"carouselExampleControls\" class=\"carousel slide\" data-ride=\"carousel\">
        <div class=\"carousel-inner text-right\">
            <div class=\"carousel-item b1 active\">
                <div class=\"container banner-info-w3-agile\">
                    <h3>Services to Grow Your Business</h3>
                    <p class=\"mt-lg-4 mt-md-3 mt-2 p-2 pl-4\">Creativity & Experience</p>
                </div>
            </div>
            <div class=\"carousel-item b2\">
                <div class=\"container banner-info-w3-agile\">
                    <h3>Various Options for Your Benefit</h3>
                    <p class=\"mt-lg-4 mt-md-3 mt-2 p-2 pl-4\">Creativity & Experience</p>
                </div>
            </div>
            <div class=\"carousel-item b3\">
                <div class=\"container banner-info-w3-agile\">
                    <h3>Enhancing Your Brand’s Appeal</h3>
                    <p class=\"mt-lg-4 mt-md-3 mt-2 p-2 pl-4\">Creativity & Experience</p>
                </div>
            </div>
            <div class=\"carousel-item b4\">
                <div class=\"container banner-info-w3-agile\">
                    <h3>Converting Concepts into Reality</h3>
                    <p class=\"mt-lg-4 mt-md-3 mt-2 p-2 pl-4\">Creativity & Experience</p>
                </div>
            </div>
        </div>
        <a class=\"carousel-control-prev\" href=\"#carouselExampleControls\" role=\"button\" data-slide=\"prev\">
            <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
            <span class=\"sr-only\">Previous</span>
        </a>
        <a class=\"carousel-control-next\" href=\"#carouselExampleControls\" role=\"button\" data-slide=\"next\">
            <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
            <span class=\"sr-only\">Next</span>
        </a>
    </div>
</section>
<!-- //banner -->
<!--about-->
<section class=\"banner-btm-section py-5\" id=\"about\">
    <div class=\"container py-xl-5 py-sm-3\">
        <h5 class=\"main-w3l-title mb-sm-4 mb-3\">Our Strategy</h5>
        <div class=\"srategy-text mb-5\">
            <p class=\"paragraph-agileinfo\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer quis tristique est, et egestas odio. Mauris ac tristique
                arcu, sed interdum risus.Integer quis tristique est, et egestas odio. Mauris ac tristique arcu, sed interdum risus.
            </p>
        </div>
        <div class=\"bnr-btm-main row justify-content-around\">
            <div class=\"col-lg-4 banner-btm-grids\">
                <div class=\"bnr-btm-info bb1 p-4\">
                    <i class=\"fab fa-gg text-white mb-3 ml-3\"></i>
                    <h3 class=\"subheading-wthree mb-md-4 mb-3\">Personal Branding</h3>
                    <p class=\"paragraph-agileinfo text-white\">Maecenas interdum, metus vitae tincidunt porttitor, magna quam egestas sem, ac scelerisque nisl nibh vel lacus.</p>
                    <span>1</span>
                </div>
            </div>
            <div class=\"col-lg-4 banner-btm-grids my-lg-0 my-4\">
                <div class=\"bnr-btm-info bb2 p-4\">
                    <i class=\"fab fa-hubspot text-white mb-3 ml-3\"></i>
                    <h3 class=\"subheading-wthree mb-md-4 mb-3\">Business Consulting</h3>
                    <p class=\"paragraph-agileinfo text-white\">Maecenas interdum, metus vitae tincidunt porttitor, magna quam egestas sem, ac scelerisque nisl nibh vel lacus.</p>
                    <span>2</span>
                </div>
            </div>
            <div class=\"col-lg-4 banner-btm-grids\">
                <div class=\"bnr-btm-info bb3 p-4\">
                    <i class=\"fab fa-connectdevelop text-white mb-3 ml-3\"></i>
                    <h3 class=\"subheading-wthree mb-md-4 mb-3\">Online Marketing</h3>
                    <p class=\"paragraph-agileinfo text-white\">Maecenas interdum, metus vitae tincidunt porttitor, magna quam egestas sem, ac scelerisque nisl nibh vel lacus.</p>
                    <span>3</span>
                </div>
            </div>
        </div>
    </div>
</section>
<!--//about-->
<!-- stats -->
<section class=\"stats py-5\">
    <div class=\"container py-xl-5 py-sm-3\">
        <h5 class=\"main-w3l-title mb-sm-4 mb-3 text-white\">Our Stats</h5>
        <div class=\"row stats_inner\">
            <div class=\"col-lg-3 col-sm-6 stat-grids\">
                <i class=\"fas fa-chart-line\"></i>
                <p class=\"counter\">2,879</p>
                <div class=\"stats-text\">
                    <h3 class=\"text-white\">Projects Done</h3>
                </div>
            </div>
            <div class=\"col-lg-3 col-sm-6 stat-grids mt-sm-0 mt-4\">
                <i class=\"fas fa-user\"></i>
                <p class=\"counter\">350</p>
                <div class=\"stats-text\">
                    <h3 class=\"text-white\">Team</h3>
                </div>
            </div>
            <div class=\"col-lg-3 col-sm-6 stat-grids mt-lg-0 mt-4\">
                <i class=\"fas fa-thumbs-up\"></i>
                <p class=\"counter\">1995</p>
                <div class=\"stats-text\">
                    <h3 class=\"text-white\">Excellent works</h3>
                </div>
            </div>
            <div class=\"col-lg-3 col-sm-6 stat-grids mt-lg-0 mt-4\">
                <i class=\"fas fa-users\"></i>
                <p class=\"counter\">1,546</p>
                <div class=\"stats-text\">
                    <h3 class=\"text-white\">Clients</h3>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- //stats -->
<!-- services -->
<section class=\"News-section py-5\">
    <div class=\"container py-xl-5 py-sm-3\">
        <h5 class=\"main-w3l-title mb-sm-4 mb-3\">Dernières offres</h5>
        <div class=\"row\">
            <div class=\"col-md-4 w3_agile_services_grid\">
                <div class=\"agile_services_grid\">
                    <div class=\"hover06 column\">
                        <div>
                            <a href=\"single.html\">
                                <img src=\"{{ asset('images/h1.jpg') }}\" class=\"img-fluid\" alt=\"Responsive image\">
                            </a>
                        </div>
                    </div>
                    <div class=\"agile_services_grid_pos\">
                        <span class=\"py-2 px-3\">06 June</span>
                    </div>
                </div>
                <h4 class=\"mt-3 mb-2\">
                    <a href=\"single.html\">Lorem ipsum dolor sit amet.</a>
                </h4>
                <p class=\"paragraph-agileinfo\">Suspendisse dapibus felis mauris, id efficitur lacus tincidunt id.</p>
                <a class=\"btn btn-primary mt-3\" href=\"single.html\" role=\"button\">Read More</a>
            </div>
            <div class=\"col-md-4 w3_agile_services_grid my-md-0 my-4\">
                <div class=\"agile_services_grid\">
                    <div class=\"hover06 column\">
                        <div>
                            <a href=\"single.html\">
                                <img src=\"{{ asset('images/h3.jpg') }}\" class=\"img-fluid\" alt=\"Responsive image\">
                            </a>
                        </div>
                    </div>
                    <div class=\"agile_services_grid_pos\">
                        <span class=\"py-2 px-3\">23 may</span>
                    </div>
                </div>
                <h4 class=\"mt-3 mb-2\">
                    <a href=\"single.html\">Lorem ipsum dolor sit amet.</a>
                </h4>
                <p class=\"paragraph-agileinfo\">Suspendisse dapibus felis mauris, id efficitur lacus tincidunt id.</p>
                <a class=\"btn btn-primary mt-3\" href=\"single.html\" role=\"button\">Read More</a>
            </div>
            <div class=\"col-md-4 w3_agile_services_grid\">
                <div class=\"agile_services_grid\">
                    <div class=\"hover06 column\">
                        <div>
                            <a href=\"single.html\">
                                <img src=\"{{ asset('images/h6.jpg') }}\" class=\"img-fluid\" alt=\"Responsive image\">
                            </a>
                        </div>
                    </div>
                    <div class=\"agile_services_grid_pos\">
                        <span class=\"py-2 px-3\">10 may</span>
                    </div>
                </div>
                <h4 class=\"mt-3 mb-2\">
                    <a href=\"single.html\">Lorem ipsum dolor sit amet.</a>
                </h4>
                <p class=\"paragraph-agileinfo\">Suspendisse dapibus felis mauris, id efficitur lacus tincidunt id.</p>
                <a class=\"btn btn-primary mt-3\" href=\"single.html\" role=\"button\">Read More</a>
            </div>
            <div class=\"col-md-6 w3_agile_services_grid mt-md-5 mt-4\">
                <div class=\"agile_services_grid\">
                    <div class=\"hover06 column\">
                        <div>
                            <a href=\"single.html\">
                                <img src=\"{{ asset('images/h4.jpg') }}\" class=\"img-fluid\" alt=\"Responsive image\">
                            </a>
                        </div>
                    </div>
                    <div class=\"agile_services_grid_pos\">
                        <span class=\"py-2 px-3\">30 April</span>
                    </div>
                </div>
                <h4 class=\"mt-3 mb-2\">
                    <a href=\"single.html\">Lorem ipsum dolor sit amet.</a>
                </h4>
                <p class=\"paragraph-agileinfo\">Duis libero mi, accumsan eu elementum in, tempus quis tortor. Mauris a iaculis dolor. Nunc sagittis, metus vitae blandit
                    fermentum
                </p>
                <a class=\"btn btn-primary mt-3\" href=\"single.html\" role=\"button\">Read More</a>
            </div>
            <div class=\"col-md-6 w3_agile_services_grid mt-md-5 mt-4\">
                <div class=\"agile_services_grid\">
                    <div class=\"hover06 column\">
                        <div>
                            <a href=\"single.html\">
                                <img src=\"{{ asset('images/h7.jpg') }}\" class=\"img-fluid\" alt=\"Responsive image\">
                            </a>
                        </div>
                    </div>
                    <div class=\"agile_services_grid_pos\">
                        <span class=\"py-2 px-3\">21 April</span>
                    </div>
                </div>
                <h4 class=\"mt-3 mb-2\">
                    <a href=\"single.html\">Lorem ipsum dolor sit amet.</a>
                </h4>
                <p class=\"paragraph-agileinfo\">Duis libero mi, accumsan eu elementum in, tempus quis tortor. Mauris a iaculis dolor. Nunc sagittis, metus vitae blandit
                    fermentum
                </p>
                <a class=\"btn btn-primary mt-3\" href=\"single.html\" role=\"button\">Read More</a>
            </div>

        </div>
    </div>
</section>
<!-- //services -->



<!-- Footer -->

<!-- //Footer -->
<!-- copyright -->
<div class=\"copyright-w3layouts\">
    <div class=\"container\">
        <p class=\"py-xl-4 py-3\">© 2018 Brisk Biz . All Rights Reserved | Design by
            <a href=\"http://w3layouts.com/\"> W3layouts </a>
        </p>
    </div>
</div>
<!-- //copyright -->
<!-- Login Responsable Modal -->
<div class=\"modal fade\" id=\"exampleModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel1\" aria-hidden=\"true\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <h5 class=\"modal-title\" id=\"exampleModalLabel1\">Login</h5>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span>
                </button>
            </div>
            <div class=\"modal-body\">
                <div class=\"login p-md-5 p-4\">
                    <form action=\"{{ path('loginAssurance') }}\" method=\"POST\">
                        <div class=\"form-group\">
                            <label>Login</label>
                            <input type=\"email\" name=\"login\" class=\"form-control\" id=\"exampleInputEmail1\" aria-describedby=\"emailHelp\" placeholder=\"\" required=\"\">
                            <small id=\"emailHelp\" class=\"form-text text-muted\">Nous ne partagerons jamais votre email avec qui que ce soit.</small>
                        </div>
                        <div class=\"form-group\">
                            <label>Mot de passe</label>
                            <input type=\"password\" name=\"mdp\" class=\"form-control\" id=\"exampleInputPassword1\" placeholder=\"\" required=\"\">
                        </div>


                        <button type=\"submit\" class=\"btn btn-primary submit mb-4\">Se connecter</button>

                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
<!--// Login Responsable Modal -->
<!-- Login Modal -->
<div class=\"modal fade\" id=\"exampleModal1\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel1\" aria-hidden=\"true\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <h5 class=\"modal-title\" id=\"exampleModalLabel1\">Login</h5>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span>
                </button>
            </div>
            <div class=\"modal-body\">
                <div class=\"login p-md-5 p-4\">
                    <form action=\"{{ path('login') }}\" method=\"POST\">
                        <div class=\"form-group\">
                            <label>Login</label>
                            <input type=\"email\" name=\"login\" class=\"form-control\" id=\"exampleInputEmail1\" aria-describedby=\"emailHelp\" placeholder=\"\" required=\"\">
                            <small id=\"emailHelp\" class=\"form-text text-muted\">Nous ne partagerons jamais votre email avec qui que ce soit.</small>
                        </div>
                        <div class=\"form-group\">
                            <label>Mot de passe</label>
                            <input type=\"password\" name=\"mdp\" class=\"form-control\" id=\"exampleInputPassword1\" placeholder=\"\" required=\"\">
                        </div>
                        <div class=\"form-group\">
                            <label>Agence</label>

                            <select class=\"form-control\" name=\"agence\">
                                {% for i in m %}
                                    <option value=\"{{ i.agence }}\">{{ i.agence }}</option>
                                {% endfor %}
                            </select>
                        </div>

                        <button type=\"submit\" class=\"btn btn-primary submit mb-4\">Se connecter</button>

                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
<!--// Login Modal -->
<!-- Register Modal -->
<div class=\"modal fade\" id=\"exampleModal2\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel2\" aria-hidden=\"true\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <h5 class=\"modal-title\" id=\"exampleModalLabel2\">Inscription</h5>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span>
                </button>
            </div>
            <div class=\"modal-body\">
                <div class=\"login p-md-5 p-4\">
                    <form action=\"{{ path('inscrire') }}\" method=\"POST\">
                        <div class=\"form-row\">
                            <div class=\"form-group col-md-6 mb-3\">
                                <label>Login</label>

                                <input type=\"text\" name=\"login\" class=\"form-control\" id=\"validationDefault01\" placeholder=\"\" required=\"\">
                            </div>
                            <div class=\"form-group col-md-6 mb-3\">
                                <label>agence</label>

                                <select name=\"agence\" class=\"form-control\">
                                    {% for i in m %}
                                        <option value=\"{{ i.agence }}\">{{ i.agence }}</option>
                                    {% endfor %}
                                </select>
                            </div>
                        </div>
                        <div class=\"form-row\">
                            <div class=\"form-group col-md-6 mb-3\">
                                <label>Nom</label>

                                <input type=\"text\"  name=\"nom\" class=\"form-control\" id=\"validationDefault01\" placeholder=\"\" required=\"\">
                            </div>


                            <div class=\"form-group col-md-6 mb-3\">
                                <label>Prénom</label>

                                <input type=\"text\" name=\"prenom\" class=\"form-control\" id=\"validationDefault01\" placeholder=\"\" required=\"\">
                            </div>
                        </div>
                        <div class=\"form-row\">
                            <div class=\"form-group col-md-6 mb-3\">
                                <label>Mot de passe</label>

                                <input type=\"password\" name=\"mdp\" class=\"form-control\" id=\"validationDefault01\" placeholder=\"\" required=\"\">
                            </div>


                            <div class=\"form-group col-md-6 mb-3\">
                                <label>Confirmer le mot de passe</label>

                                <input type=\"password\" name=\"cmdp\" class=\"form-control\" id=\"validationDefault01\" placeholder=\"\" required=\"\">
                            </div>
                        </div>
                        <div class=\"form-row\">
                            <div class=\"form-group col-md-6 mb-3\">
                                <label>Date de naissance</label>

                                <input type=\"date\" name=\"date\" class=\"form-control\" id=\"validationDefault01\" placeholder=\"\" required=\"\">
                            </div>


                            <div class=\"form-group col-md-6 mb-3\">
                                <label>Téléphone</label>

                                <input type=\"text\" name=\"tel\" class=\"form-control\" id=\"validationDefault01\" placeholder=\"\" required=\"\">
                            </div>
                        </div>




                        <button type=\"submit\" class=\"btn btn-primary submit mb-4\">S'inscrire</button>

                    </form>

                </div>

            </div>
        </div>
    </div>
</div>
<!--// Register Modal -->
<!-- Required common Js -->
<script src=\"{{ asset('js/jquery-2.2.3.min.js') }}\"></script>
<!-- //Required common Js -->
<!-- stats -->
<script src=\"{{ asset('js/jquery.waypoints.min.js') }}\"></script>
<script src=\"{{ asset('js/jquery.countup.js') }}\"></script>
<script>
    \$('.counter').countUp();
</script>
<!-- //stats -->
<!-- flexSlider -->
<script defer src=\"{{ asset('js/jquery.flexslider.js') }}\"></script>
<script>
    \$(window).load(function () {
        \$('.flexslider').flexslider({
            animation: \"slide\",
            start: function (slider) {
                \$('body').removeClass('loading');
            }
        });
    });
</script>

<!-- //flexSlider -->

<!-- password-script -->
<script>
    window.onload = function () {
        document.getElementById(\"password1\").onchange = validatePassword;
        document.getElementById(\"password2\").onchange = validatePassword;
    }

    function validatePassword() {
        var pass2 = document.getElementById(\"password2\").value;
        var pass1 = document.getElementById(\"password1\").value;
        if (pass1 != pass2)
            document.getElementById(\"password2\").setCustomValidity(\"Passwords Don't Match\");
        else
            document.getElementById(\"password2\").setCustomValidity('');
        //empty string means no validation error
    }
</script>
<!-- //password-script -->

<!-- start-smoth-scrolling -->
<script src=\"{{ asset('js/move-top.js') }}\"></script>
<script src=\"{{ asset('js/easing.js') }}\"></script>
<script>
    jQuery(document).ready(function (\$) {
        \$(\".scroll\").click(function (event) {
            event.preventDefault();
            \$('html,body').animate({
                scrollTop: \$(this.hash).offset().top
            }, 1000);
        });
    });
</script>
<!-- start-smoth-scrolling -->
<!-- here stars scrolling icon -->
<script>
    \$(document).ready(function () {
        /*
         var defaults = {
         containerID: 'toTop', // fading element id
         containerHoverID: 'toTopHover', // fading element hover id
         scrollSpeed: 1200,
         easingType: 'linear'
         };
         */

        \$().UItoTop({
            easingType: 'easeOutQuart'
        });

    });
</script>
<!-- //here ends scrolling icon -->
<!-- Js for bootstrap working-->
<script src=\"{{ asset('js/bootstrap.min.js') }}\"></script>
<!-- //Js for bootstrap working -->
</body>

</html>", "@usertest/Accueil.html.twig", "C:\\wamp64\\www\\untitled\\src\\usertestBundle\\Resources\\views\\Accueil.html.twig");
    }
}
