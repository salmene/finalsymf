<?php
namespace usertestBundle\Repository;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository{


    public function recherche(){
        $req=$this->getEntityManager()
                  ->createQuery("select u from usertestBundle:User u group by u.agence");
        return $req->getResult();
    }

    public function login($login,$mdp,$agence,$r){
        $req=$this->getEntityManager()
            ->createQuery("select u from usertestBundle:User u WHERE u.agence=:agence and u.email=:login and u.password=:mdp and u.roles LIKE :r")
            ->setParameter('agence',$agence)
            ->setParameter('login',$login)
            ->setParameter('mdp',$mdp)
        ->setParameter('r', '%"' . $r . '"%');
         return $req->getResult();
    }
    public function getIdByLogin($log){
        $req=$this->getEntityManager()
            ->createQuery("select u.id from usertestBundle:User u WHERE u.email=:login")
            ->setParameter('login',$log);
        return $req->getResult();
    }




    public function loginass($login,$mdp,$r){
        $req=$this->getEntityManager()
            ->createQuery("select u from usertestBundle:User u WHERE  u.email=:login and u.password=:mdp and u.roles LIKE :r ")

            ->setParameter('login',$login)
            ->setParameter('mdp',$mdp)
        ->setParameter('r', '%"' . $r . '"%');

        return $req->getResult();
    }

}
