<?php

namespace OffreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use OffreBundle\Entity\Offre;
use Symfony\Component\HttpFoundation\Request;
use OffreBundle\Repository\OffreRepository;


class OffreController extends Controller
{
    public function AjoutAction(Request $req){
        $offre = new Offre();
        if ($req->isMethod("POST")) {
            $offre->setTypeOffre($req->get('type'));
            $offre->setDescriptionOffre($req->get('desc'));
            $offre->setPrix($req->get('prix'));
            $offre->setAgeMax($req->get('agemax'));
            $offre->setAgeMin($req->get('agemin'));
            $em = $this->getDoctrine()->getManager();
            $session = $req->getSession();
            $id=$em->getRepository("usertestBundle:User")->getIdByLogin($session->get('log'));
            settype($id,"integer");
            $offre->setIdAssurance($id);
            $em->persist($offre);
            $em->flush();
            $offre=$em->getRepository("OffreBundle:Offre")->rechercheOffAss($id);
            return $this->render('@Offre/OffresAss.html.twig',array('Offre'=>$offre));
        }
        $em = $this->getDoctrine()->getManager();
        $session = $req->getSession();
        $id=$em->getRepository("usertestBundle:User")->getIdByLogin($session->get('log'));
        settype($id,"integer");
        $offre=$em->getRepository("OffreBundle:Offre")->rechercheOffAss($id);
        return $this->render('@usertest/Accueil.html.twig',array('Offre'=>$offre));
    }

 /*   public function tousOffresAgenceAction(Request $req ){
        $em=$this->getDoctrine()->getManager();
        $session = $req->getSession();
        $id=$session->get('log');
        settype($id,"integer");
        $offre=$em->getRepository("OffreBundle:Offre")->findBy($id);
       return $this->render('@Offre/OffresAss.html.twig',array('Offre'=>$offre));

    }*/




}
